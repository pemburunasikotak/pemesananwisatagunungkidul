package ediWahyudiaji.pemesananjasapemanduwisata

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.gson.Gson
import ediWahyudiaji.pemesananjasapemanduwisata.model.Pemilik
import ediWahyudiaji.pemesananjasapemanduwisata.pemilik.Home_pemilik_utama
import kotlinx.android.synthetic.main.layout_login_admin.*


class LoginAdmin: AppCompatActivity() {
    var sharedPreferences: SharedPreferences? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_login_admin)
        //Button login ketika di klik
        sharedPreferences = getSharedPreferences("user_details", MODE_PRIVATE);
        sharedPreferences?.contains("email");
        btn_masuk_layout_login_admin.setOnClickListener {
            //menjalankan Fungsi Login
            login()
//            System.out.println("HAHAHHAHHA")
        }

    }

    private fun login() {
        ///awal faldasi data
        if (input_text_email_login_admin.text.toString().isEmpty()){
            input_text_email_login_admin.error = "masukkan Email"
            input_text_email_login_admin.requestFocus()
        }

        if (input_text_password_login_admin.text.toString().isEmpty()){
            input_text_password_login_admin.error ="masukkan Paswd yang benar"
            input_text_password_login_admin.requestFocus()
            return
        }
        //ahir Falidasi data

        val editor = sharedPreferences!!.edit()



        //deklarasi variabel yang digunakan
        val user =input_text_email_login_admin.text.toString().trim()
        val pasword= input_text_password_login_admin.text.toString()

        //query ke database firebase table Admin dengan chilnya adalah email
        var query = FirebaseDatabase.getInstance().getReference("Admin").orderByChild("email").equalTo(user)

        System.out.println("TESTS yuhu"+pasword+user)
        query.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    for (snap in snapshot.children) {
                        val x = snap.getValue(Pemilik::class.java)
                        Log.e("testUSER", Gson().toJson(x))
                        //Cek pAssword login
                        if (x!!.password.equals(pasword.trim())) {
                            val bundel = Bundle()
                            bundel.putString("user", user);
                            val intent = Intent(this@LoginAdmin, Home_pemilik_utama::class.java)
                            intent.putExtras(bundel);
                            startActivity(intent)
                        } else {
                            Toast.makeText(this@LoginAdmin, "User Tidak Ditemukan", Toast.LENGTH_LONG).show()
                        }
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
        }
        )
    }
}