package ediWahyudiaji.pemesananjasapemanduwisata

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_deskripsi_tempat_wisata.*

class DeskripsiTempatWisata : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_deskripsi_tempat_wisata)
        val bundle = intent.extras
        tvJudulDetail.setText((bundle?.getString("nama")))
        tvDetailKontent.setText((bundle?.getString("detail")))
        id.setText((bundle?.getString("id")))
        nama.setText((bundle?.getString("nama")))

        bundle?.getString("gambar")?.let { Log.e("Lihat", it) }

        Glide.with(this)
            .load(bundle?.getString("gambar"))
            .placeholder(R.drawable.bg_profil)
            .error(R.drawable.bg_profil)
            .into(imageDetail);
        btnLokasi.setOnClickListener(){
            val intent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse(bundle?.getString("maps"))
            )
            startActivity(intent)
        }
        btnYoutube.setOnClickListener(){
            val intent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse(bundle?.getString("youtube"))
            )
            startActivity(intent)
        }
        btn_pesan.setOnClickListener(){
//            val intent = Intent(this, PesanWisata::class.java);
            val bundel = Bundle()
            bundel.putString("id", bundle?.getString("id"))
            bundel.putString("nama", bundle?.getString("nama"))
            bundel.putString("maps", bundle?.getString("maps"))
            bundel.putString("detail", bundle?.getString("detail"))
            bundel.putString("gambar",bundle?.getString("gambar"))
            bundel.putString("listpantai",bundle?.getString("listpantai"))
            val intent = Intent(this, PesanWisata::class.java);
            intent.putExtras(bundel)
            startActivity(intent)
        }
    }
}