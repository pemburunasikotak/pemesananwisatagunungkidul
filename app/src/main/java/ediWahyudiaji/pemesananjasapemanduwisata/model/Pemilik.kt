package ediWahyudiaji.pemesananjasapemanduwisata.model

import com.google.gson.annotations.SerializedName
import ediWahyudiaji.pemesananjasapemanduwisata.Users


data class Pemilik(
    var id: String= "",
    var email: String = "",
    var password: String ="",
    var nama: String="",
    var no_telp: String="",
    var jenisKelamin: String="",
    var image:String="",
)