package ediWahyudiaji.pemesananjasapemanduwisata.model

class Pesanan(
    var id: String= "",
    var diambil: String ="",
    var nama: String = "",
    var alamat: String ="",
    var durasi: String = "",
    var lokasi: String= "",
    var email: String="",
    var noTelf: String="",
    var statusbayar: String="",
)