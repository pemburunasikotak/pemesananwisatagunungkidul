package ediWahyudiaji.pemesananjasapemanduwisata

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import ediWahyudiaji.pemesananjasapemanduwisata.model.Pemilik
import ediWahyudiaji.pemesananjasapemanduwisata.model.Pesanan
import ediWahyudiaji.pemesananjasapemanduwisata.pemesan.Home_pemesan_utama
import kotlinx.android.synthetic.main.activity_pesan_wisata.*
import java.util.*

class PesanWisata : AppCompatActivity() {
    lateinit var ref : DatabaseReference
    private var timePickerDialog: TimePickerDialog? = null
    private lateinit var db: DatabaseReference
    private var listPemandu: MutableList<String> = mutableListOf()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pesan_wisata)
        ref = FirebaseDatabase.getInstance().getReference("Pesanan")

        val bundle = intent.extras
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        edit_text_wisata.setText((bundle?.getString("listpantai")))
        getUserProfile()

        btn_pesan_wisata.setOnClickListener(){
            BuatPesanan();
        }
        btnKalender.setOnClickListener(){
            val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, month, day ->
                // Display Selected date in TextVie
                edit_text_durasi.setText("" +   day + " " + (month+1) + ", " + year)
            }, year, month, day)
            dpd.show()
        }
//        db = FirebaseDatabase.getInstance().getReference("Admin")
        var query = FirebaseDatabase.getInstance().getReference("Admin")
        query.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    for (snap in snapshot.children) {
                        val x = snap.getValue(Pemilik::class.java)
                        val json = Gson().toJson(x!!)
                        val gson = Gson()
//                        val mapType = object : TypeToken<Map<String, Any>>() {}.type
                        var maping: Map<String, Any> = gson.fromJson(json, object : TypeToken<Map<Any, Any>>() {}.type)
//                        listMobil.add(Gson().toJson(x!!.nama_mobil.toString()))
                        maping.forEach {
                            if(it.key.equals("nama")){
                                listPemandu.add(it.value.toString())
                            }
                        }
                    }
                    SpinerPemandu()
                }
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
        })
    }

    private fun SpinerPemandu() {
        val datamob = listPemandu
        val spinner = findViewById<Spinner>(R.id.spinnerPemandu)
        if (spinner != null) {
            val adapter = ArrayAdapter(this,
                android.R.layout.simple_spinner_item, datamob)
            spinner.adapter = adapter
            spinner.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>,
                                            view: View, position: Int, id: Long) {
                    Log.e("DISINI", (datamob[position]))
                    nama_pemandu_wisata.setText(datamob[position])
                }
                override fun onNothingSelected(parent: AdapterView<*>) {
                    // write code to perform some action
                }
            }
        }
    }

    fun getUserProfile() {
        val dataRef = FirebaseDatabase.getInstance().getReference("USER")
        var user: Users? = null
        val currentuser = FirebaseAuth.getInstance().currentUser?.email.toString()
        dataRef.orderByChild("email").equalTo(currentuser)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                    Log.d("apaini", user?.nama!!)
                }
                override fun onDataChange(p0: DataSnapshot) {
                    for (snap in p0.children) {
                        user = snap.getValue(Users::class.java)
                    }

                    edit_noTelf.text = user!!.no_telp
                    et_email.text = user!!.email
                    edit_text_nama_pemesan.setText(user!!.nama)
                }
            })
    }

    private fun BuatPesanan() {
        if (edit_text_nama_pemesan.text.toString().isEmpty()){
            edit_text_nama_pemesan.error = "masukkan Nama Pemesan"
            edit_text_nama_pemesan.requestFocus()
            return
        }
        if (edit_text_alamat.text.toString().isEmpty()){
            edit_text_alamat.error  = "Masukkan Alamat Pemesan"
            edit_text_alamat.requestFocus()
            return
        }
        if (edit_text_durasi.text.toString().isEmpty()){
            edit_text_nama_pemesan.error = "masukkan Durasi"
            edit_text_nama_pemesan.requestFocus()
            return
        }
        if (edit_text_wisata.text.toString().isEmpty()){
            edit_text_alamat.error  = "Masukkan Lokasi Wisata "
            edit_text_alamat.requestFocus()
            return
        }
//        if(nama_pemandu_wisata.text.toString().isEmpty()){
//            nama_pemandu_wisata.error  = "Pilih Pemandu "
//            nama_pemandu_wisata.requestFocus()
//            return
//        }
        if (edit_text_nama_pemesan.text.toString().isNotEmpty() && edit_text_alamat.text.toString().isNotEmpty() && edit_text_wisata.text.toString().isNotEmpty() && edit_text_durasi.text.toString().isNotEmpty()){
            val id = ref.push().key.toString()
            val nama = edit_text_nama_pemesan.text.toString().trim()
            val alamat = edit_text_alamat.text.toString().trim()
            val durasi = edit_text_durasi.text.toString().trim()
            val lokasi = edit_text_wisata.text.toString().trim();
            val noTelf = edit_noTelf.text.toString().trim();
            val email = et_email.text.toString().trim();
            val pemandu = nama_pemandu_wisata.text.toString().trim();

            val event = Pesanan(id,pemandu, nama,alamat, durasi, lokasi, email, noTelf)
            ref.child(id).setValue(event).addOnCompleteListener {
                Toast.makeText(this, "Sukses buat pesanan ", Toast.LENGTH_LONG).show()
                val intent = Intent(this, Home_pemesan_utama::class.java)
                startActivity(intent)
            }
        }
    }
}