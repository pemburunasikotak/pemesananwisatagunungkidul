package ediWahyudiaji.pemesananjasapemanduwisata.pemesan

import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import ediWahyudiaji.pemesananjasapemanduwisata.R
import ediWahyudiaji.pemesananjasapemanduwisata.Users
import ediWahyudiaji.pemesananjasapemanduwisata.model.Pesanan
import kotlinx.android.synthetic.main.fragment_home_pemesan.*


class Home_pemesan : Fragment() {


    private var list : MutableList<Pesanan> = ArrayList()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home_pemesan, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_wisata_pemesan.setOnClickListener(){
            Toast.makeText(context, "hahahahaha", Toast.LENGTH_LONG);
        }
        btn_pesan_pemesan.setOnClickListener(){
//            val intent = Intent(context, Pesanan_pemesan::class.java)
//            context?.startActivity(intent)
        }
        btn_peta_pemesan.setOnClickListener(){
            val intent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://www.google.com/maps/search/pantai+gunung+kidul/@-8.1561074,110.5716839,13z/data=!3m1!4b1")
            )
            context?.startActivity(intent)
        }
        rv_listDaftarPesanan.setHasFixedSize(true)
        getUserProfile();

    }
    fun getUserProfile() {

        val dataRef = FirebaseDatabase.getInstance().getReference("USER")
        var user: Users? = null
        val currentuser = FirebaseAuth.getInstance().currentUser?.email.toString()
//        val currentuser = "akakomjanti@gmail.com"

        dataRef.orderByChild("email").equalTo(currentuser)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                    Log.d("apaini", user?.nama!!)
                }

                override fun onDataChange(p0: DataSnapshot) {
                    for (snap in p0.children) {
                        user = snap.getValue(Users::class.java)
                    }
//                    tvemailhomemember.text = user!!.email
                    tvemailhomemember.setText(user!!.email);
                    tv_namaUser.setText(user!!.nama)
                    fungsiRecyleView();
                }
            })

    }

    private fun fungsiRecyleView() {
        val listadapter = context?.let { AdapterDataPesan(it,list) }
        rv_listDaftarPesanan.adapter = listadapter
        rv_listDaftarPesanan.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        //deklarasi untuk Database
        val nama = tvemailhomemember.text.toString().trim()
        var myRef = FirebaseDatabase.getInstance().getReference("Pesanan").ref
        //isi data di RV
        Log.e("CEK NAMA",nama )
        var query =myRef.orderByChild("email").equalTo(nama)
        query.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                for (snap in snapshot.children) {
                    val x = snap.getValue(Pesanan::class.java)
//                    Log.e("TEST", Gson().toJson(x))
                    x?.let { list.add(it) }
                    listadapter?.notifyDataSetChanged()
                }
            }
            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(context, "Terjai kesalahan ketika inisialisasi database", Toast.LENGTH_SHORT).show()
            }
        })

    }


    companion object {
        fun newInstance():Home_pemesan{
            val fragment = Home_pemesan()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}