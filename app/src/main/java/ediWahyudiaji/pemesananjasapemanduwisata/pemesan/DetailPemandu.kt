package ediWahyudiaji.pemesananjasapemanduwisata.pemesan

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.squareup.picasso.Picasso
import ediWahyudiaji.pemesananjasapemanduwisata.R
import ediWahyudiaji.pemesananjasapemanduwisata.model.Pemilik
import kotlinx.android.synthetic.main.activity_detail_pemandu.*

class DetailPemandu : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_pemandu)
        val bundle = intent.extras
        idDetail.setText((bundle?.getString("id")))
        namaDetail.setText((bundle?.getString("nama")))
        notelfDetail.setText((bundle?.getString("notelf")))
        lokasiDetail.setText((bundle?.getString("lokasi")))
        durasiDetail.setText((bundle?.getString("durasi")))
        alamatDetail.setText((bundle?.getString("alamat")))
        emailDetail.setText((bundle?.getString("email")))
        name_pemandu.setText((bundle?.getString("pemandu")))

        var query = FirebaseDatabase.getInstance().getReference("Admin").orderByChild("nama").equalTo((bundle?.getString("pemandu").toString()))
        query.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    Log.e("Test", "Masuk")
                    for (snap in snapshot.children) {
                        val x = snap.getValue(Pemilik::class.java)
                        email_pemandu.setText(x!!.email)
                        name_pemandu.setText(x!!.nama)
                        no_telp_pemandu.setText(x!!.no_telp)
                        jenis_kelamin_pemandu.setText((x!!.jenisKelamin))
                        Picasso.get()
                            .load(x!!.image)
                            .centerCrop()
                            .fit()
                            .into(imagePemandu)
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
        })
    }
}