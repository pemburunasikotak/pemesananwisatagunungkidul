package ediWahyudiaji.pemesananjasapemanduwisata.pemesan


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.gson.Gson
import ediWahyudiaji.pemesananjasapemanduwisata.R
import kotlinx.android.synthetic.main.fragment_pesanan_pemesan.*


@Suppress("UNREACHABLE_CODE", "DEPRECATION")
class Pesanan_pemesan : Fragment() {
    private var list : MutableList<ModelDaftarPantai> = ArrayList()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pesanan_pemesan, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rv_tempatWisata.setHasFixedSize(true)
       fungsiRecyleView()
    }
    private fun fungsiRecyleView() {
        val listadapter = context?.let { Adapter(it,list) }
        rv_tempatWisata.adapter = listadapter
        rv_tempatWisata.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        //deklarasi untuk Database
        var myRef = FirebaseDatabase.getInstance().getReference("Wisata")
        //isi data di RV
        myRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                for (snap in snapshot.children) {
                    val x = snap.getValue(ModelDaftarPantai::class.java)
                    Log.e("TEST", Gson().toJson(x))
                    list.add(x!!)
                    listadapter?.notifyDataSetChanged()
                }
            }
            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(context, "Terjai kesalahan ketika inisialisasi database", Toast.LENGTH_SHORT).show()
            }
        })
    }
    companion object {
        fun newInstance(): Pesanan_pemesan {
            val fragment =
                Pesanan_pemesan()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}