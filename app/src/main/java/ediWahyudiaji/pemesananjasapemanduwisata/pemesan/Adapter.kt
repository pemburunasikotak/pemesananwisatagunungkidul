package ediWahyudiaji.pemesananjasapemanduwisata.pemesan

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.text.Html
import android.util.Log
import android.util.SparseBooleanArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import ediWahyudiaji.pemesananjasapemanduwisata.DeskripsiTempatWisata
import ediWahyudiaji.pemesananjasapemanduwisata.R


class Adapter(private val context: Context, private val list: MutableList<ModelDaftarPantai>)
    : RecyclerView.Adapter<Adapter.HomePemesanHolder>() {

    private var ListPantai: MutableList<String> = mutableListOf()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Adapter.HomePemesanHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(
            R.layout.list_tempat_wisata, parent, false
        )
        return HomePemesanHolder(view)
    }

    override fun onBindViewHolder(holder: Adapter.HomePemesanHolder, position: Int) {
        val list = list[position]
        holder.tv_name.text = list.nama

        holder.tv_name.setOnClickListener {
            val bundel = Bundle()
            ListPantai.joinToString(
                prefix = "",
                separator = ",",
                postfix = "",
                truncated = "...",
                transform = { it.lowercase() }
            )
            bundel.putString("id", list.id)
            bundel.putString("nama", list.nama)
            bundel.putString("maps", list.maps)
            bundel.putString("detail", list.detail)
            bundel.putString("gambar",list.gambar)
            bundel.putString("youtube", list.youtube)
            bundel.putString("listpantai",ListPantai.toString())
//            val intent = Intent(context, PesanWisata::class.java)
            val intent = Intent(context, DeskripsiTempatWisata::class.java);
            intent.putExtras(bundel)
            context.startActivity(intent)
        }
        holder.chackbox.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked){
                ListPantai.add(list.nama)
                Toast.makeText(context,ListPantai.toString(),Toast.LENGTH_SHORT).show()
            }else {
                ListPantai.remove(list.nama)
                Toast.makeText(context,ListPantai.toString(),Toast.LENGTH_SHORT).show()
            }
        }

    }

    override fun getItemCount(): Int = list.size

    inner class HomePemesanHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_name: TextView = view.findViewById(R.id.tv_listPantai)
        val chackbox: CheckBox = view.findViewById(R.id.checkBox)
    }
}