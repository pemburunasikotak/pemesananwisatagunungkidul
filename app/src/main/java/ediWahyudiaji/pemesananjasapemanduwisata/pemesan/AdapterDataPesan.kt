package ediWahyudiaji.pemesananjasapemanduwisata.pemesan

import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.*
import ediWahyudiaji.pemesananjasapemanduwisata.Login_pilih
import ediWahyudiaji.pemesananjasapemanduwisata.R
import ediWahyudiaji.pemesananjasapemanduwisata.model.Pesanan

class AdapterDataPesan(private val context: Context, private val list: MutableList<Pesanan>)
    : RecyclerView.Adapter<AdapterDataPesan.HomePemesanHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterDataPesan.HomePemesanHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(
            R.layout.list_daftar_pesanan_member, parent, false
        )
        return HomePemesanHolder(view)
    }

    override fun onBindViewHolder(holder: AdapterDataPesan.HomePemesanHolder, position: Int) {
        val list = list[position]
        holder.tv_name.text = list.nama
        holder.tv_tujuan.text = list.lokasi
        holder.itemView.setOnClickListener {
            //Toast.makeText(context,"Pantai"+list.nama,Toast.LENGTH_LONG)
        }
        holder.id.text = list.id;
        holder.hapus.setOnClickListener(){
           // Toast.makeText(context,"TEST Hapus",Toast.LENGTH_LONG)
            val ref = FirebaseDatabase.getInstance().getReference("Pesanan").child(list.id)
            val Query: Query = ref
            Query.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    for (appleSnapshot in dataSnapshot.children) {
                        appleSnapshot.ref.removeValue()
                    }
                }
                override fun onCancelled(databaseError: DatabaseError) {
                    Log.e(ContentValues.TAG, "onCancelled", databaseError.toException())
                }
            })
            val intent = Intent(context, Home_pemesan_utama::class.java)
            context.startActivity(intent)
        }
        holder.detail.setOnClickListener(){
            val bundel = Bundle()
            bundel.putString("id",list.id)
            bundel.putString("nama",list.nama)
            bundel.putString("notelf",list.noTelf)
            bundel.putString("lokasi",list.lokasi)
            bundel.putString("durasi",list.durasi)
            bundel.putString("alamat",list.alamat)
            bundel.putString("email",list.email)
            bundel.putString("pemandu",list.diambil)
            val intent = Intent(context, DetailPemandu::class.java)
            intent.putExtras(bundel)
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int = list.size

    inner class HomePemesanHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_name: TextView = view.findViewById(R.id.tv_alamat_pemesan)
        val tv_tujuan: TextView= view.findViewById(R.id.tv_tujuan_pemesan)
        val hapus: Button = view.findViewById(R.id.btn_hapusPesanan);
        val id : TextView = view.findViewById(R.id.tv_id);
        val detail: TextView = view.findViewById(R.id.btn_detail);

    }
}