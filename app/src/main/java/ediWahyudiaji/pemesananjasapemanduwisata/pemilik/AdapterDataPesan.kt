package ediWahyudiaji.pemesananjasapemanduwisata.pemilik

import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.*
import ediWahyudiaji.pemesananjasapemanduwisata.R
import ediWahyudiaji.pemesananjasapemanduwisata.model.Pesanan
import ediWahyudiaji.pemesananjasapemanduwisata.pemesan.Home_pemesan_utama

class AdapterDataPesan(private val context: Context, private val list: MutableList<Pesanan>, private val email: String)
    : RecyclerView.Adapter<AdapterDataPesan.HomePemesanHolder>() {
    lateinit var ref : DatabaseReference

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterDataPesan.HomePemesanHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(
            R.layout.item_pesanan_pemilik, parent, false
        )
        return HomePemesanHolder(view)
    }

    override fun onBindViewHolder(holder: AdapterDataPesan.HomePemesanHolder, position: Int) {
        val list = list[position]
        val email = email;
        ref = FirebaseDatabase.getInstance().getReference("PesananDiambil")
        holder.tv_name.text = list.nama
        holder.tv_tujuan.text = list.lokasi
        holder.itemView.setOnClickListener {
            Toast.makeText(context,"Pantai"+list.nama,Toast.LENGTH_LONG)
        }
        holder.durasi.text= list.durasi.toString()
        holder.ambil.setOnClickListener(){
            val event = Pesanan(list.id, email, list.nama, list.alamat, list.durasi, list.lokasi, list.email, list.noTelf)
            ref.child(list.id).setValue(event).addOnCompleteListener {
                hapus(list.id)
            }

        }
    }

    private fun hapus(id:String) {
        val ref = FirebaseDatabase.getInstance().getReference("Pesanan").child(id)
        val Query: Query = ref
        Query.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (appleSnapshot in dataSnapshot.children) {
                    appleSnapshot.ref.removeValue()
                }
            }
            override fun onCancelled(databaseError: DatabaseError) {
                Log.e(ContentValues.TAG, "onCancelled", databaseError.toException())
            }
        })
        val bundel = Bundle()
        bundel.putString("user", email);
        val intent = Intent(context, Home_pemilik_utama::class.java)
        intent.putExtras(bundel);
        context.startActivity(intent)
    }

    override fun getItemCount(): Int = list.size

    inner class HomePemesanHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_name: TextView = view.findViewById(R.id.item_nama_recyclerview)
        val tv_tujuan: TextView= view.findViewById(R.id.item_alamat_recyclerview)
        val durasi: TextView= view.findViewById(R.id.item_durasi)
        val ambil: Button = view.findViewById(R.id.btn_ambil);
//        val id : TextView = view.findViewById(R.id.tv_id);

    }
}