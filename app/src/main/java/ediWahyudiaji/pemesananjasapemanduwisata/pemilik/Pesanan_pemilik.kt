package ediWahyudiaji.pemesananjasapemanduwisata.pemilik


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import ediWahyudiaji.pemesananjasapemanduwisata.R
import ediWahyudiaji.pemesananjasapemanduwisata.model.Pesanan
import kotlinx.android.synthetic.main.fragment_pesanan_pemilik.*


@Suppress("UNREACHABLE_CODE", "DEPRECATION")
class Pesanan_pemilik : Fragment() {
    private var email: String? =null;
    private var list : MutableList<Pesanan> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        email = arguments?.getString("user");
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pesanan_pemilik, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rv_layout_pesan_pemilik.setHasFixedSize(true)
        fungsiRecyleView()
    }
    private fun fungsiRecyleView() {
        val listadapter = context?.let { AdapterDataPesan(it, list, email.toString()) }
        rv_layout_pesan_pemilik.adapter = listadapter
        rv_layout_pesan_pemilik.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        //deklarasi untuk Database
        var myRef = FirebaseDatabase.getInstance().getReference("Pesanan")
        //isi data di RV
        myRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                for (snap in snapshot.children) {
                    val x = snap.getValue(Pesanan::class.java)
//                    Log.e("TEST", Gson().toJson(x))
                    list.add(x!!)
                    listadapter?.notifyDataSetChanged()
                }
            }
            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(context, "Terjai kesalahan ketika inisialisasi database", Toast.LENGTH_SHORT).show()
            }
        })
    }

    companion object {
        fun newInstance(email: String): Pesanan_pemilik {
            val fragment =
                Pesanan_pemilik()
            val args = Bundle()
            args.putString("user", email)
            fragment.arguments = args
            return fragment
        }
    }
}