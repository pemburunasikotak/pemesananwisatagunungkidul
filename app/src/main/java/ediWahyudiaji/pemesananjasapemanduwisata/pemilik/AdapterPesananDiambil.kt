package ediWahyudiaji.pemesananjasapemanduwisata.pemilik

import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.*
import ediWahyudiaji.pemesananjasapemanduwisata.R
import ediWahyudiaji.pemesananjasapemanduwisata.model.Pesanan

class AdapterPesananDiambil(private val context: Context, private val list: MutableList<Pesanan>,val email: String)
    : RecyclerView.Adapter<AdapterPesananDiambil.HomePemesanHolder>() {
    lateinit var ref : DatabaseReference

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterPesananDiambil.HomePemesanHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(
            R.layout.list_daftar_pesanan_pemilik, parent, false
        )
        return HomePemesanHolder(view)
    }

    override fun onBindViewHolder(holder: AdapterPesananDiambil.HomePemesanHolder, position: Int) {
        val list = list[position]
        ref = FirebaseDatabase.getInstance().getReference("PesananDiambil")
        holder.tv_name.text = list.nama
        holder.tv_alamat.text = list.lokasi
        holder.itemView.setOnClickListener {
            Toast.makeText(context,"Pantai"+list.nama,Toast.LENGTH_LONG)
        }
        holder.durasi.text= list.durasi.toString()
        holder.selesai.setOnClickListener(){
            val path = "PesananDiambil"
            ref = FirebaseDatabase.getInstance().getReference("PesananSelesai")
            val event = Pesanan(list.id, list.nama, list.alamat, list.durasi, list.lokasi, list.email, list.noTelf)
            ref.child(list.id).setValue(event).addOnCompleteListener {
                hapus(list.id,path )
            }
        }
        holder.chat.setOnClickListener(){
//            val nama = list.alamat
            val  i = Intent(Intent.ACTION_VIEW, Uri.parse("https://api.whatsapp.com/send?phone=62${list.noTelf}&text=${list.nama}&Apakah%20Pesanan%20Sudah%20Sesuai"))
            context.startActivity(i)
        }
        holder.batal.setOnClickListener(){
            val path = "PesananDiambil"
            ref = FirebaseDatabase.getInstance().getReference("Pesanan")
            val event = Pesanan(list.id, list.nama, list.alamat, list.durasi, list.lokasi, list.email, list.noTelf)
            ref.child(list.id).setValue(event).addOnCompleteListener {
                hapus(list.id,path )
            }
        }
        if (list.statusbayar =="true"){
            val red = Color.parseColor("#D9F8C4")
            holder.linierlayoutlist.setBackgroundColor(red)
        }

        holder.sudahTransfer.setOnClickListener(){
            ref = FirebaseDatabase.getInstance().getReference("PesananDiambil")
            val statusbayar = "true";
            val event = Pesanan(list.id,list.diambil,list.nama,list.alamat,list.durasi, list.lokasi, list.email, list.noTelf, statusbayar)
            ref.child(list.id).setValue(event).addOnCompleteListener {
                Log.e("",event.statusbayar)
                Toast.makeText(context, "Sukses Rubah Status", Toast.LENGTH_LONG).show();
                val intent = Intent(context,Home_pemilik_utama::class.java);
                context.startActivity(intent);
            }
        }

    }

    private fun hapus(id: String, path: String) {
        val ref = FirebaseDatabase.getInstance().getReference(path).child(id)
        val Query: Query = ref
        Query.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (appleSnapshot in dataSnapshot.children) {
                    appleSnapshot.ref.removeValue()
                }
            }
            override fun onCancelled(databaseError: DatabaseError) {
                Log.e(ContentValues.TAG, "onCancelled", databaseError.toException())
            }
        })
//        val intent = Intent(context, Home_pemilik_utama::class.java)
//        context.startActivity(intent)
        val bundel = Bundle()
        bundel.putString("user", email);
        val intent = Intent(context, Home_pemilik_utama::class.java)
        intent.putExtras(bundel);
        context.startActivity(intent)
    }


    override fun getItemCount(): Int = list.size

    inner class HomePemesanHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_name: TextView = view.findViewById(R.id.list_nama)
        val tv_alamat: TextView= view.findViewById(R.id.list_alamat)
        val durasi: TextView= view.findViewById(R.id.list_paket)
        val selesai: Button = view.findViewById(R.id.btn_listSelesai);
        val chat: Button= view.findViewById(R.id.btn_listchat)
        val batal: Button= view.findViewById(R.id.btn_listBatal)
        val sudahTransfer: Button= view.findViewById(R.id.btn_sudah_transfer)
        val linierlayoutlist: LinearLayout = view.findViewById(R.id.linierlayoutlist);


    }
}