package ediWahyudiaji.pemesananjasapemanduwisata.pemilik

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import ediWahyudiaji.pemesananjasapemanduwisata.R
import ediWahyudiaji.pemesananjasapemanduwisata.Users
import ediWahyudiaji.pemesananjasapemanduwisata.model.Pesanan
import ediWahyudiaji.pemesananjasapemanduwisata.pemesan.AdapterDataPesan
import kotlinx.android.synthetic.main.activity_deskripsi_tempat_wisata.*
import kotlinx.android.synthetic.main.fragment_home_pemesan.*
import kotlinx.android.synthetic.main.fragment_home_pemilik.*

class Home_pemilik : Fragment() {
    var sharedPreferences: SharedPreferences? = null
    private var list : MutableList<Pesanan> = ArrayList()
    private var email: String? =null;
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        email = arguments?.getString("user");

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home_pemilik, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rv_listDaftarPesananDiambil.setHasFixedSize(true);

//        recyleView()
        getUserProfile()
    }
    fun getUserProfile() {
        val dataRef = FirebaseDatabase.getInstance().getReference("Admin")
        var user: Users? = null
        val currentuser = email.toString();

        dataRef.orderByChild("email").equalTo(currentuser)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                    Log.d("apaini", user?.nama!!)
                }
                override fun onDataChange(p0: DataSnapshot) {
                    for (snap in p0.children) {
                        user = snap.getValue(Users::class.java)
                    }
                    tvemailhomepemilik.setText(user!!.email);
                    tv_nama_home_pemilik.setText(user!!.nama)
                    recyleView()
                }
            })
    }

    private fun recyleView() {
        val listadapter = context?.let { AdapterPesananDiambil(it,list, email.toString()) }
        rv_listDaftarPesananDiambil.adapter = listadapter
        rv_listDaftarPesananDiambil.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        //deklarasi untuk Database
        val email = tvemailhomepemilik.text.toString().trim()
        var myRef = FirebaseDatabase.getInstance().getReference("PesananDiambil").ref
        //isi data di RV
        Log.e("CEK NAMA RECYLE",email )
        var query =myRef.orderByChild("diambil").equalTo(email)
        query.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                for (snap in snapshot.children) {
                    val x = snap.getValue(Pesanan::class.java)
                    x?.let { list.add(it) }
                    listadapter?.notifyDataSetChanged()
                }
            }
            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(context, "Terjai kesalahan ketika inisialisasi database", Toast.LENGTH_SHORT).show()
            }
        })
    }
    companion object {
        fun newInstance(email: String):Home_pemilik{
            val fragment = Home_pemilik()
            val args = Bundle()
            args.putString("user", email)
            fragment.arguments = args
            return fragment
        }
    }
}