package ediWahyudiaji.pemesananjasapemanduwisata.pemilik

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import ediWahyudiaji.pemesananjasapemanduwisata.Login
import ediWahyudiaji.pemesananjasapemanduwisata.Login_pilih
import ediWahyudiaji.pemesananjasapemanduwisata.R
import ediWahyudiaji.pemesananjasapemanduwisata.Users
import kotlinx.android.synthetic.main.fragment_profil_pemilik.*


class Profil_pemilik : Fragment() {
    private var email: String? =null;
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        email = arguments?.getString("user");
        Log.e("EMAILLL",email.toString());
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profil_pemilik, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getUserProfile()
        btn_keluar_profilPemilik.setOnClickListener(){
            logout();
        }
    }
    fun getUserProfile() {
        val dataRef = FirebaseDatabase.getInstance().getReference("Admin")
        var user: Users? = null
//        val currentuser = FirebaseAuth.getInstance().currentUser?.email.toString()
        val currentuser = email
        dataRef.orderByChild("email").equalTo(currentuser)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                    Log.d("apaini", user?.nama!!)
                }

                override fun onDataChange(p0: DataSnapshot) {
                    for (snap in p0.children) {
                        user = snap.getValue(Users::class.java)
                    }
                    tvnotelfon_profil_pemilik_bawah.text = user!!.no_telp
                    edt_phonepemilik.setText(user!!.no_telp)
                    tvemail_profil_pemilik_bawah.text = user!!.email
                    tvemail_profil_pemilik.text = user!!.email
                    tvnama_profil_pemilik.text = user!!.nama

                    btn_edit_pemilik.setOnClickListener {
                        if (edt_phonepemilik.visibility == View.VISIBLE) {
                            if (edt_phonepemilik.text.toString()
                                    .equals("")
                            ) edt_phonepemilik.setError("Isi terlebih dahulu")
                            else {
//                                App.instance.editUser(edt_phone.text.toString())
                                tvnotelfon_profil_pemilik_bawah.setText(edt_phonepemilik.text.toString())
                                btn_edit_pemilik.setText("EDIT")
                                edt_phonepemilik.visibility = View.GONE
                                tvnotelfon_profil_pemilik_bawah.visibility = View.VISIBLE
                            }
                        } else {
                            btn_edit_pemilik.setText("SIMPAN")
                            edt_phonepemilik.visibility = View.VISIBLE
                            tvnotelfon_profil_pemilik_bawah.visibility = View.GONE
                        }
                    }
                }
            })
    }
    fun logout(){
        context?.let { FirebaseApp.initializeApp(it) }
        FirebaseAuth.getInstance().signOut()
        requireActivity().run{
            startActivity(Intent(this, Login_pilih::class.java))
            finish()
        }
    }

//
    companion object {
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(email: String):Profil_pemilik{
            val fragment = Profil_pemilik()
            val args = Bundle()
            args.putString("user", email)
            fragment.arguments = args
            return fragment
        }
    }
}